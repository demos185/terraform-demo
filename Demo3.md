## Demo Project:
Terraform & AWS EKS
## Technologies used:
Terraform, AWS EKS, Docker, Linux, Git
## Project Decription:
* Automate provisioning EKS cluster with Terraform
## Description in details:
__Create evething from scratch__

__Step 1:__ Create VPC
1. Create VPC using  an existing [module](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest)
2. create new file `vpc.tf` and paste module 
```go

variable vpc_cidr_block {}
variable publick_subnet_cidr_blocks {}
variable private_subnet_cidr_blocks {}

module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.1"

  name = "myapp-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  publick_suvnets = var.publick_subnet_cidr_blocks
}
```
__Note 1:__ Module code gets downloaded on `terraform init`

__Best practice:__ 1 private and 1 public subnet in each AZ
>- name: name_of_your_vpc

>Define variables in `terraform.tfvars`
>```go
>vpc_cidr_block = "10.0.0.0/16"
>publick_subnet_cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
>private_subnet_cidr_blocks = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
>```

3. Add AZS into this module
   * create `data`

```go
data "aws_avialability_zones" "azs" {}
```
4. Add provider
```go
provaider "aws" {
    region = "eu-west-3"
}
```
5. Add AZS inside module
```go 
module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.1"

  name = "myapp-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  publick_suvnets = var.publick_subnet_cidr_blocks
  azs = data.aws_avialability_zones.azs.names

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  public_subnets = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }
  private_subnets = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}
```


>__tags - use cases__
>- for human consumption to have more information
>- label for referencing components from other components (progrommatically)
>- e.g. for Cloud Controller Manager

>__c-c-m - Cloud Controller manager:__ - orchestrate, connecting to the VPCs, connecting to the subnets, connecting with the worker nodes, and all this configuration  - talking to the resources in your AWS account and creating some stuff
>- c-c-m needs to know which resources in your account it should talk to. It need to know which VPC should be used in a cluster, which subnet should be used in the cluster because you may have multple VPCs and multiple subnets. You need to tell the control plane or AWS, use these VPCs and these subnets for this specific cluster

>For Private and Public subnets, you need one more tag respectively
>- create another tag `kubernetes.io/role/elb` and `kubernetes.io/role/internal-elb`  - They are __required!__
>- those texts to identify the right components and to difference between public and private subnets
>- If you don't specify them,  those controlles can't actually identify the resources then you will get an error

__Step 2:__ execute `terraform init`


### Part 2
__Step 1:__ Create EKS cluster
1. Create new file `eks-cluster.tf` 
2. Use module for EKS [link](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest)
3. Paste basic syntax for this module
```go
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.0.4"
}
```
4. Setting attributes one by one
```go
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.0.4"

  cluster_name = "myapp-eks-cluster"
  cluster_version = "1.25"


  subnet_ids = module.myapp-vpc.private_subnets
  vpc_id = module.myapp-vpc.vpc_id

  tags = {
    environment = "development"
    application = "myapp"
  }
}
```
- `cluster_version` - version of kubernetes
- `subnet_ids`- (reference anoter module for it)
  - __Private:__ your Workload
  - __Public:__ external resources like loadbalancer 
- `tags:environment` you don't specify somethin specific here it's just add something meaninful for you
- `vpc_id` - (reference anoter module for it)
- 

>__Important:__ Read official docs to understand what requrment attributes you need, and how to fulfill them!

5. Configure worker nodes (in this example used self-managed ) 
   
Paste it in EKS module and configure it(for syntax use docs):
```go 
tags = {
    ...
}

eks_managed_node_groups = {
    blue = {}
    green = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    }
}
```
__Note:__ Mind aboit __Pricing__ and don't forget __destroy__ not needed components!

6. terraform plan --> terraform apply 
### Part 3
__Step 1:__ Deploy nginx-app into your cluster
1. connect to your cluster using kubectl
   * move `kubeconfig` file inside `.kube` folder (you can do this automatically using AWS command)
```sh
aws eks update-kubeconfig --name NAME_OF_YOURR_CLUSTER --region YOUR_CLUSTER_REGION
```
2. Execute `kubectl get node` to check everything is working
3. Apply nginx (from previous demos or yu can find it in folder for yhis demo)
```sh
kubectl apply -f PATH_TO_YOUR_CONFIG
```
