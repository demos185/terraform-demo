## Demo Project:
Configure a Shared Remote State
## Technologies used:
Terraform, AWS S3
## Project Decription:
* Configure Amazon S3 as remote storage for Terraform state
## Description in details:

__Step 1:__ Configure Remote Strorage
1. Open your `main.tf` file and new block on the top of file


```go
terraform {
    required_version = ">= 0.12"
    backend "s3" {
        bucket = "myapp-bucket"
        key = "myapp/state.tfstate"
        region = "eu-west-3"
    }
}
```
__Beckends:__
- determines how state is loaded/stored
- default: local storage    

__S3:__ - it's a storage in the AWS that is mostly used for storing files
- `bucket` - name ofn bucket
- `key` -  path inside your bucket
 
 __It's not gonna work because first you have to create a bucket of course, because it doesn't exist yet__

__Step 2:__ Create S3 Bucket
1. Open AWS UI and open `S3` service
2. Create Bucket:
   - name: name of bucket
   - region: (it doesnt' have to be the same region as the one that you are creating  your resources in)
   - Block public accsess: you can configure access here
   - Bucket versioning: create versioning of the files that are stored in the bucket (Good practice: Enable it)
   - Tags: optional
   - Default encryption: 
   - Create


>__Important:__ before creating these changes, make sure to destoy your current infrastracture!

__Step 3:__ Execute Jenkins Pipeline
1. Execute pipeline
2. In logs you can see that resources got created