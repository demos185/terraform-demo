## Demo Project:
Automate AWS Infrastructure
## Technologies used:
Terraform, AWS, Docker, Linux, Git
## Project Decription:
* Create TF project to automate provisioning AWS Infrastructure and its components, such as: VPC, Subnet, RouteTable, Internet Gateway, EC2, Security Group
* Configure TF script to automate deploying Docker container to EC2 instance
## Description in details:
### Create TF project to automate provisioning AWS Infrastructure and its components, such as: VPC, Subnet, RouteTable, Internet Gateway, EC2, Security Group
__Step 1:__ VPC and Subnet

1. Create variables for vpc, availability-zone and subnet
```go
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
```
2. Configure vpc and subnet blocks

VPC
```go
resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = var.cidr_blocks[0].name
    }
}
```
Subnet
```go
resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name = var.cidr_blocks[1].name
    }
}
```
3. change name tag
```go
variable env_prefix {}
```
now add created variable inside blocks

VPC
```go
tags = {
  Name = "${var.env_prefix}-vpc"
}
```
Subnet
```go
tags = {
  Name = "${var.env_prefix}-subnet-1"
}
```
4. Assign values to created variables in `terraform.tfvars`
```go
vpc_cidr_block = "10.0.0.0/16"
subnet_cidr_block = "10.0.10.0/24"
avail_zone = "eu-west-3b"
env_prefix = "dev"
```
__Step 2:__ Route Table & Internet Gateway

1. Create new Route table using two routes, one for internal one for external connections

Create route table
```go
resource "aws_route_table" "myapp-route-table" {
    vpc_id = aws_vpc.myapp-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name = "${var.env_prefix}-rtb"
    }
}
```
Create intetner gateway
```go
resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    tags = {
        Name = "${var.env_prefix}-igw"
    }
}
```
__Note:__ It doesn't matter which order you wrote resources because `Terraform` will understand which order resources mustbe installed

__Step 3:__ Subnet Association with Route table

__Note:__ You created a route table inside your VPC. However, you need to associate subnets with the route table, so that traffic within the subnet can also be handled by the route table

1. Create resource `aws_route_table_association`
```go
resource "aws_route_table_association" "a-rtb-subnet" {
    subnet_id = aws_subnet.myapp-subnet-1.id
    route_table_id = aws_route_table.myapp-route-table.id
}
```
__Alternative:__ If you want to use the default route table:
1. Remove created association and route table resource
2. configure default rout table
```go
resource "aws_default_route_table" "main-rtb"{
    default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name = "${var.env_prefix}-main-rtb"
    }
}
```
>- default_route_table_id: you can see ID using `terraform state show aws_vpc.myapp-vpc` Syntax: `terraform state show OBJECT`

__Step 4:__ Security Group
1. Create Security Group and open 2 ports on server(22 for SSH. 8080 for internal)
```go
resource "aws_seurity_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["YOUR_IP_FOR_EXAMPLE/32"]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids  = []
    }

    tags = {
        Name = "${var.env_prefix}-sg"
    }
}
```

>1. Ingress: you can configure range of ports here for incoming traffic
>2. cidr_block: this is a range or a list of IP addresses or cidr blocks that are allowed to access port defined in here. You can use variables here
>3. egress: for exiting traffic

__Step 5:__ AWS instance block
1. Create AWS  EC2 instance 
```go 
resource "aws_instance" "myapp-server" {
    ami = ID_OF_IMAGE (or example: data.aws_ami.latest-amazon-linux-image.id)
    instance_type = var.instance_type


    (OPTIONAL_CONFIGURATIONS_BELOW)
    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg]
    availability_zone = var.avail_zone
    associate_public_ip_adress = true (FOR SSH CONECTION)
    key_name = "YOUR_SERVER_KEY PAIR" (You can create key_pair and dowload on your machine. Change permissions and move to /.ssh folder ro reference on it)
    tags = {
        Name = "${var.env_prefix}-server"
    }
}
```
__Note:__ If you dont specify optional configurations they will be launched in a default VPC

>- __ami__: maybe you want to fetch latest version (Example: amazon linux 2) you need to create another block
>```go
>data "aws_ami" "latest-amazon-linux-image"
>   most_recent = true
>   owners = ["amazon"]
>   filter {
>       name = "name"
>       values = "amzn2-ami-hvm-*-x86_64-gp2"
>   }
>   filter { 
>       name = "vitrualization-type"
>       values = ["hvm"]
>   }
>```
>- __owners:__ here you can choose provisioner
>- __filters:__ define the criteria for this these queres. You can make filters as many as you want  
>- __instance_type:__ you can define it on top in variables

2. Output the results of this query so that you can test whether your filter criteria are set correctly 
```go
output "aws_ami" {
    value = data.aws_ami.latest-amazon-linux-image
}
output "ec2_public_ip" {
    value = aws_instance.myapp-server.public_ip
}

```
3. Assign values in `terraform.tfvars`

__Step 6:__ Automate SSH key pair 
1. Create resource `aws_key_pair`
```go
resource "aws_key_pair" "ssh-key" {
    keny_name = "server-key"
    public_key = "YOUR_PUBLIC_KEY"  [Better use variable: file(var.publick_key_location) ]
}
```
>__Notes:__ 
>- a key pair must already __exist__ localy on your machine
>- you already generated a key pair for Git repo or DO Droplet
>- "YOUR_PUBLIC_KEY" : add here everything that contains inside your public key   (do __NOT__ check this into your Git repo) 
>- publick_key_location :  Use absolute path!

Now you can reference this key pair in the AWS EC2 instance now
```go
resource "aws_instance" "myapp-server" {
    ami = ID_OF_IMAGE (or example: data.aws_ami.latest-amazon-linux-image.id)
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg]
    availability_zone = var.avail_zone

    associate_public_ip_adress = true
    key_name = aws_key_pair.ssh-key.key_name

    tags = {
        Name = "${var.env_prefix}-server"
    }
}
```
### Configure TF script to automate deploying Docker container to EC2 instance

__Step 1:__ Install Docker on server and configure it
1. Create user data inside `aws_instance` block

```go
resource "aws_instance" "myapp-server" {
    ami = ID_OF_IMAGE (or example: data.aws_ami.latest-amazon-linux-image.id)
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg]
    availability_zone = var.avail_zone

    associate_public_ip_adress = true
    key_name = aws_key_pair.ssh-key.key_name
    user_data = <<EOF
                    #!/bin/bash
                    sudo yum update -y && sudo yum install -y docker
                    sudo systemctl start docker
                    sudo usermode -aG docker ec2-user
                    docker run -p 8080:80 nginx
                EOF
    tags = {
        Name = "${var.env_prefix}-server"
    }
}
```
__Note 1:__ `user_data` will be executed once on server

__Step 2:__ Exctract to shell script

If you have much longer or more complicated script that you want execute as an entry point script when the server starts, you can also __reference__ it from a file instead of having it inside

1. Detele content inside `user_data` and pass file location
```go
user_data = file("entry-script.sh")
```
__Note:__ Better let another tools like `Ansible` manage applications because cant track what's happend on the server using scripts
