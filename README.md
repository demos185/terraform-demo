## List of Demos:

__1.__ Automate AWS Infrastructure

__2.__ Modularize Project

__3.__ Terraform & AWS EKS

__4.__ Configure a Shared Remote State      

__5.__ Complete CI/CD with Terraform

## Folders with code for Demos:
__deploy-to-EC2-deffault:__ Demo#1 Demo#2

__deploy-to-ec2:__ Demo#1

__modulation:__ Demo#2

__eks:__ Demo#3

__remote-state:__ Demo#4 Demo#5