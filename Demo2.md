## Demo Project:
Modularize Project
## Technologies used:
Terraform, AWS, Docker, Linux, Git
## Project Decription:
* Divide Terraform resources in to reusable modules
## Description in details:
__Note:__ __Many__ resources/code in just 1 main.tg file isn't good. Better divide it on modules!

__Step 1:__  Clean `main.tf`
  1. Create file `outputs.tf` for output. Cut output from `main.tf` and paste into created file
  2. Create `variables.tf` for variables. Cut variables from `main.tf` and paste them into created file for variables
   
__Note 1:__ You can call that files whatever you want

__Step 2:__ Create module
1. Create folder `modules` and create folders inside it like `subnet`, `webserver`.  __EVERY__ module has own `main.tf` file
2. Create main.tf file another files like `output.tf` for every folder
3. extract configurations from root `main.tf` to created modules
4. Make modules completely configurable - change values on variables
5. configure `variables.tf` files with required variables in them  

__Note:__ __Group multiple resources__ into a __logical__ unit (not just for onne or two resources) 

__Step 3:__ Use modules
1. In root `main.tf` call configurations from another `main.tf` files using keyword `main`
```go
module "./myapp-subnet" {
    source= "/modules/subnet"
    subnet_cidr_block = var.sidr_block
}
```
>- myapp-subnet: you can call module whatever you want!
>- source: Here you need define path to the `main.tf` file
>- subnet_cidr_block: if you want use variable here, you need to reference it from the tf vars file from root module, you need create that variable definition also in the `variables.tf` in that module. Variables from root are set through the `terraform.tfvars`  where you providing values for all those variables. That means:
>   * __Values are defined in .tfvars file --> set as value in variables.tf in root --> values are passed to child module as arguments --> via variables.tf in child module__

__Step 4:__ Module Output

How do you access the resources of a child modle?
>__Output Values__
>- like return value of module
>- to __expose/export resource attributes__ to parent module

1. Go to your module and open `outputs.tf` file and 

```go
output "subnet" {
    value = aws_subnet.my-app-subnet-1
}
```
2. Grab name of output ("subnet") and reference that subnet in the instance resource which is in another module using following syntax:
```go
subnet_id = module.myapp-subnet.subnet.id
```
>__Syntax:__ subnet_id = module.NAME_OF_MODULE.OUTPUT_NAME.id

__Note:__ If you need ourput in module from another modele . you can reference using same  syntax using `module.NAME_OF_MODULE...`

__Step 5:__ Apply Configuration Changes
1. Use command `terraform init` - whenever module added/changed!


