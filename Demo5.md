## Demo Project:
Complete CI/CD with Terraform
## Technologies used:
Terraform, Jenkins, Docker, AWS, Git, Java, Maven, Linux, DockerHub
## Project Decription:
Integrate provisioning stage into complete CI/CD Pipeline to automate provisioning server instead of deploying to an existing server
  * Create SSH Key Pair
  * Install Terraform inside Jenkins container
  * Add Terraform configuration to application’s git repository
  * Adjust Jenkinsfile to add “provision” step to the CI/CD pipeline that provisions EC2 instance
  * So the complete CI/CD project we build has the following configuration:
     1. CI step: Build artifact for Java Maven application
     2. CI step: Build and push Docker image to DockerHub
     3. CD step: Automatically provision EC2 instance using TF
     4. CD step: Deploy new application version on the provisioned EC2 instance with Docker Compose
## Description in details:
  
### Create SSH Key Pair
__Step 1:__ Create Key pair manualy inside AWS and give it to Jenkins

1. Go to AWS UI EC2/Key pairs and create key
    * Name:
    * Format: pem
    * Tags: (Optional)
    * Create
2. Open Jenkins UI and add key pair into your pipeline where you gonna use this key
   * Kind: SSH username with private key
   * ID: server-ssh-key
   * Username: ec2-user
   * Private key: PASTE_YOUR_KEY_HERE
   * Create

>- Username - username that will be logging into the server with SSH

### Install Terraform inside Jenkins container
1. SSH into your server and go inside Jenkins container (as __Root__ user)
2. Install Terraform using [official guide](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

### Add Terraform configuration to application’s git repository
__Step 1:__ Create folder `terraform`inside `java-maven-app` folder and create `main.tf` file (use `main.tf` from previous demo)

_Adjust it:_

1. Remove key-pair and variable (because you created new one)
2. In `aws_instance` adjust `key_name` (just set name of new created key pair in double quotes)
3. Adjust reference on entry script (`user_data`). Just create this file with commands in `terraform` folder 
4. Adjust script: remove line with running container and add command with downloading docker compose(you can find this commands in official [docker docs](https://docs.docker.com/compose/install/other/)) and make it executable `sudo chmod +x /usr/local/bin/docker-compose`
5. Create file for variables and parameterize them with `default` values
```go
variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable subnet_cidr_block {
    default = "10.0.10.0/24"
}
variable avail_zone {
    default = "eu-west-3a"
}
variable env_prefix {
    default = "dev"
}
variable my_ip {
    default = "YOUR_IP_HERE"
}
variable jenkins_ip {
    default = "YOUR_JENKINS_IP_HERE"
}
variable instance_type {
    default = "t2.micro"
}
variable region {
    default = "eu-west-3"
}
```
6. Remove `output "aws_ami_id"` because you don't need it anymore (you can also create `outputs.tf` file and move your outputs there)      
### Adjust Jenkinsfile to add “provision” step to the CI/CD pipeline that provisions EC2 instance
__Step 1:__ Create `privision server` stage
1. define `steps` then `script`
2. You need to execute terraform commands here so you need to be inside terraform folder. You can solve it using `dir` command and provide folder
```groovy
script {
  dir('terraform') {

  }
}
```  
3. Inside `dir` you need execute commands `terraform init` and `terraform apply --auto-approve`
4. For applying you need to authenticate with AWS. Define the credentials for the AWS user (acces key id, secret access key id) as environment variables(you can hardcode it but env vars are better)
```groovy
environment {
   AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
   AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
}
```
5. Maybe you want to set the environment to test(by default we set 'dev'). You can set a value of a variable inside terraform configuration using `TF_VAR_name`
```groovy
TF_VAR_env_prefix = 'test'
```
__Step 2:__ Deploy Stage in Jenkins

1. You need to reference the attribute of terraform resource from `Jenkinsfile`. You can use `output` command in order to get the value o what's defined here. You can create environment variable for it and use it in another steps of Jenkinsfile:
```groovy
stage('provision server') {
    environment {
        AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
        AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
        TF_VAR_env_prefix = 'test'
    }
    steps {
        script {
            dir('terraform') {
                sh "terraform init"
                sh "terraform apply --auto-approve"
                EC2_PUBLIC_IP = sh(
                    script: "terraform output ec2_public_ip",
                    returnStdout: true
                ).trim()
            }
        }
    }
}
```
>- returnStdout: it prints out the value of standart output
>- .trim: you can trimm value, so if  there are any spaces before of after, it will be trimmed
2. Now you can referece that value
```groovy 
stage ('deploy') {
  steps {
    script {
      ...
      ...

      def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
      def ec2Instance = "ec2-user@${EC2_PUBLIC_IP}"
    }
  }
}
```
3. Following issue

When isntance have been created it needs some time to initialize

__Solution:__ Before the rest of the commands get executed, use `sleep` and configure time
```groovy
steps {

  script {
    echo "waiting for EC2 server to initialize" 
    sleep(time: 90, unit: "SECONDS") 
 
   ...
   ...
   ...
  }
}
```
4. Eddit sshagent
```groovy
sshagent(['server-ssh-key']) {
  sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
  sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
  sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
```
>-  -o StrictHostKeyChecking=no : eddit in every command otherwise it will give you some issues

__Step 3:__ Commit changes

__Step 4:__ Trigger pipeline
1. You can get an error because you let access only your IP but not Jenkins server so create variable for jenkins
```go
variable jenkins_ip {
  default = "YOUR_JENKINS_IP_HERE"
} 
```
2. Add this Ip to security group
```go
ingress {
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = [var.my_ip, var.jenkins_ip]
 }
```

If you want ssh to the server don't forget change permissions for .pem file!  

__Step 5:__ Execute `docker login` on the server on EC2 server

1. Open your `.sh sript` where you execute docker-compose and add here docker login command
```sh
export IMAGE=$1
export DOCKER_USER=$2
export DOCKER_PWD=$3
echo $DOCKER_PWD | docker login -u $DOCKER_USER --password-stdin
docker-compose -f docker-compose.yaml up --detach
echo "success"
```
2. Add variables to command that calls the script
```sh
stage('deploy') {
  environment {
      DOCKER_CREDS = credentials('docker-hub-repo')
  }
  steps {
      script {
          echo "waiting for EC2 server to initialize" 
          sleep(time: 90, unit: "SECONDS") 

          echo 'deploying docker image to EC2...'
          echo "${EC2_PUBLIC_IP}"

          def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"
          def ec2Instance = "ec2-user@${EC2_PUBLIC_IP}"

          ...
      }
  }
}  
```
>- `DOCKER_CREDS_USR` and `EC2_PUBLIC_IP` - they created autommatically whn you define `DOCKER_CREDS` line

__Step 6:__ Destroy
If you want destroy terraform infrastructure, the easiest way is to do it from Jenkins because you don't have terraform state localy

1. Open your pipeline, open last build, replay and delete terraform commands and add `Terraform destroy --auto-aprove` and destroy deploy step